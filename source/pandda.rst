PanDDA (CCP4: Supported Program)
================================

NAME
----

**pandda.analyse, pandda.inspect & pandda.export** - perform, model, and
export a multi-dataset crystallographic analysis

DESCRIPTION
-----------

The PanDDA (Pan-Dataset Density Analysis) method identifies
"changed-state" signal (e.g. binding ligands) in crystallographic
datasets by contrasting putative changed-state datasets against a series
of "ground-state" (e.g. unbound) datasets; this identifies regions that
are significantly different to the ground-state. Since changed states
are invariably present at sub-unitary occupancy in the crystal, the
PanDDA method includes further steps to deconvolute the superposition of
crystal states, revealing generally clear density for the changed state
of the crystal and simplifying modelling.

The analysis of the crystallographic data is performed with
**pandda.analyse**. The changed-state of the crystal can then be
modelled with **pandda.inspect** (using coot). **pandda.export** merges
the models of the bound and unbound states of the crystal to create
multi-state ensembles, and prepares the structures for refinement.
Occupancy parameter files are automatically generated to allow the
occupancies of the superposed states to be grouped in refinement.

FULL DOCUMENTATION, TUTORIAL & EXAMPLE DATA
-------------------------------------------

Full documentation and a worked example can be found at
http://pandda.bitbucket.org.

SYNOPSIS
--------

**pandda.analyse** **data\_dirs=**\ *'/path/to/directories/\*'*
**pdb\_style=**\ *'\*.filename.pdb'*
**mtz\_style=**\ *'\*.filename.mtz'* **lig\_style=**\ *'\*.cif'*
**cpus=**\ *N*

BASIC INPUT COMMANDS
--------------------

**data\_dirs** - Wild-carded path to the input directories

**pdb\_style** - The naming style of the refined pdb file in the input
directory (any wild-carded sections must match the wild-carded secions
in **data\_dirs**)

**mtz\_style** - The naming style of the refined mtz file in the input
directory if different to **pdb\_style** (any wild-carded sections must
match the wild-carded sections in **data\_dirs**)

**lig\_style** - The naming style of the cif files for any ligands that
are present in the crystal (leaving as the default will pick up all cif
files in the input directories)

**cpus** - The number of cpus to use for processing

AUTHORS
-------

Nicholas Pearce, Sebastian Kelm, Jiye Shi, Charlotte Deane & Frank von
Delft
