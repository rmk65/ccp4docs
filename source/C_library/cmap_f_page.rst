`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

Fortran API to CMAP
-------------------

File list
---------

-  `cmaplib\_f.c <cmaplib__f_8c.html>`__

Overview
--------

This library consists of a set of wrappers to the CMAP library giving
the same API as the original maplib.f For details of the API, see the
original `documentation <../maplib.html>`__. This document covers some
peculiarities of the C implementation.
