`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

mtzdata.h
=========

`Go to the documentation of this file. <mtzdata_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      mtzdata.h: Definition of MTZ data structure.
    00003      Copyright (C) 2001  CCLRC, Martyn Winn
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00021 #ifndef __CMTZData__
    00022 #define __CMTZData__
    00023 
    00024 #define MTZVERSN "MTZ:V1.1"         
    00025 #define MTZ_MAJOR_VERSN 1      
    00026 #define MTZ_MINOR_VERSN 1      
    00029 #define SIZE1 20                    
    00030 #define MTZRECORDLENGTH 80          
    00031 #define MAXSPGNAMELENGTH 20         
    00033 #define NBATCHWORDS 185       
    00034 #define NBATCHINTEGERS 29     
    00035 #define NBATCHREALS 156       
    00037 #define MXTALS      100      
    00038 #define MSETS      1000      
    00039 #define MCOLUMNS  10000      
    00042 typedef struct { char label[31];       
    00043                  char type[3];         
    00044                  int active;           
    00045                  unsigned int source;  
    00046                  float min;            
    00047                  float max;            
    00048                  float *ref;           
    00049                } MTZCOL;
    00050 
    00052 typedef struct { int setid;            
    00053                  char dname[65];       
    00054                  float wavelength;     
    00055                  int ncol;             
    00056                  MTZCOL **col;         
    00057                } MTZSET;
    00058 
    00060 typedef struct { int xtalid;           
    00061                  char xname[65];       
    00062                  char pname[65];       
    00063                  float cell[6];        
    00064                  float resmin;         
    00065                  float resmax;         
    00066                  int nset;             
    00067                  MTZSET **set;         
    00068                } MTZXTAL;
    00069 
    00071 typedef struct bathead { int num;              
    00072                  char title[71];       
    00073                  char gonlab[3][9];    
    00074                  int iortyp;           
    00076                  int lbcell[6];        
    00077                  int misflg;           
    00078                  int jumpax;           
    00080                  int ncryst;           
    00081                  int lcrflg;           
    00083                  int ldtype;           
    00085                  int jsaxs;            
    00086                  int nbscal;           
    00088                  int ngonax;           
    00089                  int lbmflg;           
    00092                  int ndet;             
    00094                  int nbsetid;          
    00095                  float cell[6];        
    00096                  float umat[9];        
    00098                  float phixyz[2][3];   
    00100                  float crydat[12];     
    00101                  float datum[3];       
    00102                  float phistt;         
    00103                  float phiend;         
    00104                  float scanax[3];      
    00105                  float time1;          
    00106                  float time2;          
    00107                  float bscale;         
    00108                  float bbfac;          
    00109                  float sdbscale;       
    00110                  float sdbfac;         
    00111                  float phirange;       
    00112                  float e1[3];          
    00114                  float e2[3];          
    00116                  float e3[3];          
    00118                  float source[3];      
    00119                  float so[3];          
    00120                  float alambd;         
    00121                  float delamb;         
    00122                  float delcor;         
    00123                  float divhd;          
    00124                  float divvd;          
    00125                  float dx[2];          
    00126                  float theta[2];       
    00127                  float detlm[2][2][2]; 
    00129                  struct bathead *next; 
    00130                } MTZBAT;
    00131 
    00133 typedef struct { int spcgrp;           
    00134                  char spcgrpname[MAXSPGNAMELENGTH+1];  
    00135                  int nsym;             
    00136                  float sym[192][4][4]; 
    00138                  int nsymp;            
    00139                  char symtyp;          
    00140                  char pgname[11];      
    00141                } SYMGRP;
    00142 
    00143 typedef union { char amnf[4]; 
    00144                 float fmnf;
    00145               } MNF;
    00146 
    00148 typedef struct { CCP4File *filein;     
    00149                  CCP4File *fileout;    
    00150                  char title[71];       
    00151                  char *hist;           
    00152                  int histlines;        
    00153                  int nxtal;            
    00154                  int ncol_read;        
    00155                  int nref;             
    00156                  int nref_filein;      
    00157                  int refs_in_memory;   
    00158                  int n_orig_bat;       
    00159                  float resmax_out;     
    00160                  float resmin_out;     
    00161                  MNF mnf;              
    00162                  SYMGRP mtzsymm;       
    00163                  MTZXTAL **xtal;       
    00164                  MTZBAT *batch;        
    00165                  MTZCOL *order[5];     
    00166                } MTZ;
    00167 
    00168 #endif

.. raw:: html

   </div>
