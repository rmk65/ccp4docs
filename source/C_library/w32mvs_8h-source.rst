`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

w32mvs.h
========

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      w32mvs.h: function prototypes for w32mvs.c functions
    00003      Copyright (C) 2003  Alun Ashton     
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00011 #ifdef _MSC_VER
    00012 
    00013 #include <windows.h>
    00014 #include <stdio.h>
    00015 #include <time.h>
    00016 
    00017 #define BUFSIZE 1024
    00018 
    00019 #endif

.. raw:: html

   </div>
