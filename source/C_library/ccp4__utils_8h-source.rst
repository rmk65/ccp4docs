`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_utils.h
=============

`Go to the documentation of this file. <ccp4__utils_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_utils.h: headers for utility functions.
    00003      Copyright (C) 2001  CCLRC, Charles Ballard
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00016 #ifndef __CCP4_UTILS
    00017 #define __CCP4_UTILS
    00018 
    00019 #include <string.h>
    00020 #include "ccp4_types.h"
    00021 #include "library_file.h"
    00022 static char rcsidh[] = "$Id$";
    00023 
    00024 #ifdef __cplusplus
    00025 namespace CCP4 {
    00026 extern "C" {
    00027 #endif
    00028 
    00029 /****************************************************************************
    00030  * Function prototypes                                                      *
    00031  ****************************************************************************/
    00032 
    00033 size_t ccp4_utils_flength (char *, int);
    00034 
    00035 int ccp4_utils_translate_mode_float(float *, const void *, int, int);
    00036 
    00037 void ccp4_utils_fatal (const char *);
    00038 
    00039 void ccp4_utils_print (const char *message);
    00040 
    00041 int ccp4_utils_setenv (char *);
    00042 
    00043 /* turn on line buffering for stdout */
    00044 int ccp4_utils_outbuf (void);
    00045 
    00046 /* turn off any buffering on stdin */
    00047 int ccp4_utils_noinpbuf (void);
    00048 
    00049 union float_uint_uchar ccp4_nan ();
    00050 
    00051 int ccp4_utils_isnan (const union float_uint_uchar *);
    00052 
    00053 void ccp4_utils_bml (int, union float_uint_uchar *);
    00054 
    00055 void ccp4_utils_wrg (int, union float_uint_uchar *, float *);
    00056 
    00057 void ccp4_utils_hgetlimits (int *, float *);
    00058 
    00059 int ccp4_utils_mkdir (const char *, const char *);
    00060 
    00061 int ccp4_utils_chmod (const char *, const char *);
    00062 
    00063 void *ccp4_utils_malloc(size_t);
    00064 
    00065 void *ccp4_utils_realloc(void *, size_t);
    00066 
    00067 void *ccp4_utils_calloc(size_t, size_t);
    00068 
    00069 int ccp4_file_size(const char *);
    00070 
    00071 char *ccp4_utils_username(void);
    00072 
    00073 char *ccp4_utils_basename(const char *filename);
    00074 
    00075 char *ccp4_utils_pathname(const char *filename);
    00076 
    00077 char *ccp4_utils_extension(const char *filename);
    00078 
    00079 char *ccp4_utils_joinfilenames(char *dir, char *file);
    00080 
    00081 void ccp4_utils_idate (int *);
    00082 
    00083 char *ccp4_utils_date(char *);
    00084 
    00085 void ccp4_utils_itime (int *);
    00086 
    00087 char *ccp4_utils_time(char *);
    00088 
    00089 float ccp4_utils_etime (float *);
    00090 
    00091 #if defined (_MSC_VER)
    00092 double ccp4_erfc( double x );
    00093 #endif
    00094 
    00095 /****************************************************************************
    00096 *  End of prototypes                                                        *
    00097 *****************************************************************************/
    00098 #ifdef __cplusplus
    00099 }
    00100 }
    00101 #endif
    00102 
    00103 #endif  /* __CCP4_UTILS */

.. raw:: html

   </div>
