`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CMTZ library
------------

.. raw:: html

   <div class="fragment">

::



       

.. raw:: html

   </div>

File list
---------

-  `cmtzlib.h <cmtzlib_8h.html>`__ - contains details of the C/C++ API
-  `mtzdata.h <mtzdata_8h.html>`__ - contains details of the
   `MTZ <structMTZ.html>`__ data structure

Overview
--------

The CMTZ library is centred around a data structure, defined in
`mtzdata.h <mtzdata_8h.html>`__ Reading an `MTZ <structMTZ.html>`__ file
causes a data structure to be created and populated from the
`MTZ <structMTZ.html>`__ header. There are a variety of functions for
manipulating the data structure, for example adding crystals, datasets
or columns. The data structure can be dumped to an output
`MTZ <structMTZ.html>`__ data file.

The library operates in two modes with respect to the reflection data.
If ``mtz->refs_in_memory = 1`` (as set e.g. by the argument to
``MtzGet``), then all reflection data is read into memory from the file,
and can be manipulated in memory. Else if ``mtz->refs_in_memory = 0``
then explicit calls to ``ccp4_lrrefl``, ``ccp4_lrreff`` and
``ccp4_lwrefl`` are required to read and write reflections from/to disk.

Information on the data structure is given in
`mtzdata.h <mtzdata_8h.html>`__

Principal Functions
-------------------

Reading MTZ files
~~~~~~~~~~~~~~~~~

Start by looking at ``MtzGet`` and ``ccp4_lrrefl`` / ``ccp4_lrreff``.

Writing MTZ files
~~~~~~~~~~~~~~~~~

If you have a structure in memory already, use ``MtzPut`` followed by
``MtzFree`` to release the memory.

If you need to create a structure from scratch (i.e. without reading
from an input file) then use ``MtzMalloc``, ``MtzOpenForWrite``,
``ccp4_lwsymm``, ``MtzAddXtal``, ``MtzAddDataset``, ``MtzAddColumn`` and
``ccp4_lwrefl``.

Symmetry Information
--------------------

All reflection data in an `MTZ <structMTZ.html>`__ file is assumed to
belong to the same spacegroup. The spacegroup is identified in the
`MTZ <structMTZ.html>`__ file by SYMINF and SYMM records in the file
header. This information is copied into the in-memory data structure.
The list of symmetry operators (copied from the SYMM header records) is
taken to be the definitive indicator of the spacegroup.

The functions ``ccp4_lrsymi``, ``ccp4_lrsymm`` and ``ccp4_lwsymm`` read
from and write to the symmetry sections of the data structure. No
symmetry manipulations are done within the CMTZ library itself. Within
CCP4, the CSYM library provides appropriate functions, but other
symmetry libraries could be used.

Examples
--------

See examples on `ftp area <ftp://ftp.ccp4.ac.uk/mdw/cmtz>`__
