`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

overview.h
==========

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      overview.h: overview of CINCH - Crystallography IN C Headers
    00003      Copyright (C) 2003  CCLRC, Martyn Winn
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */

.. raw:: html

   </div>
