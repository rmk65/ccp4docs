EXTENDS (CCP4: Unsupported Program)
===================================

NAME
----

**extends** - Extend Fourier maps and compute standard uncertainty of
electron density.

SYNOPSIS
--------

| **extends mapin** *foo\_in.map* **mapout** *foo\_out.map* [ **xyzin**
  *foo\_in.brk* ]
| [Key-worded input]

DESCRIPTION
-----------

The program **EXTENDS** generates any part of a map from a map
calculated by the **FFT** program which typically contains only one
asymmetric unit.

This is a modified version of the **EXTEND** program that attempts to
compute the standard uncertainty of the electron density.  To do this
the input map must be a true difference map (i.e. of the type Fo-Fc, not
nFo-(n-1)Fc) and must consist of **exactly one** asymmetric unit (an
integral number of a.u.'s such as **exactly** half or an **exact**
complete unit cell is also valid input).  Note that the program does
**NOT** check that the input map covers an exact integral number of
a.u.'s: it is the user's responsibility to ensure this.  However this
will normally be the case if the **FFT** program has been used to
prepare the input map.

The rationale for this is statistical rigour: the asymmetric unit of the
map represents the true population of electron density values in the
statistical sense and therefore the values of the mean and RMS deviation
for the asymmetric unit can be considered to be those of the true
population mean and population standard deviation respectively.  Any
other subset or superset of the map that is not an integral number of
asymmetric units is a sample in the statistical sense, with
corresponding sample mean and sample standard deviation.

It may even be a biased sample either because the density covers only a
specific region of the structure and is therefore non-representative of
the map as a whole, or because when a map is extended, density values
related by the map symmetry are very likely to be generated more than
once.  Sample statistics, whether biased or not, are always only
approximations to population statistics.

Note that the standard deviation of the density, unlike the RMS
deviation of the density from its mean value, is a property of the
density that does **NOT** change simply because a different map extent
is chosen.  Most other programs that output electron density maps that
do not cover an exact integral number of a.u.'s always automatically
recompute both the mean and RMSD - if this is done it is then not valid
to treat the RMSD in subsequent calculations as though it were the SU of
the density.

KEY WORDED INPUT
----------------

The data control lines are identified by keywords. Only the first 4
letters of each keyword are necessary. Some data cards are optional. The
available keywords are: XYZLIM, SYMMETRY, BORDER, FILL, KEEP and END. 
They are all optional, but some must be specified if XYZIN isn't
assigned.

DESCRIPTION OF KEYWORDS
-----------------------

XYZLIM <xmin> <xmax> <ymin> <ymax> <zmin> <zmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Output map limits in grid points or fractions of cell edge. If this card
is not given XYZIN must be assigned. The program then reads the
coordinates to determine the map limits.

SYMMETRY <Name> \| <Number>
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Followed by space group Name (e.g. P212121) or Number (e.g. 19).
Symmetry is usually read from the header of the input map so this
keyword is not normally needed. Use it only to change symmetry, or if no
symmetry is present in the map. This is needed after certain non-CCP4
programs e.g. from Uppsala.

MAPSYM <Name> \| <Number>
~~~~~~~~~~~~~~~~~~~~~~~~~

Inserts the specified space group symmetry operators into the output
map, but does not use them for extending the map. This may be needed
after \`skewing' a map, since that map will have only X,Y,Z symmetry,
but be careful.

BORDER <width>
~~~~~~~~~~~~~~

<width> of border in Angstroms to add to coordinate limits.

Default = 10 Angstroms. This card is only needed if XYZLIM is not given;
otherwise limits are determined from the XYZIN coordinate file.

FILL <fill>
~~~~~~~~~~~

Set map values outside borders of input map to <fill>.

KEEP
~~~~

Keep the input RMSD of the density.  This option should be used only for
2mFo-DFc and other types of non-difference Fouriers, *i.e.* with
coefficients of the form nFo-(n-1)Fc.  In some cases the program can
detect that a non-difference Fourier has been input and will not
recompute the standard uncertainty; however it would not be wise to rely
on this!

The RMSD of a 2mFo-DFc or equivalent map is completely devoid of any
statistical meaning anyway: it will greatly depend on the solvent
content, so comparing Fourier maps contoured at 1 sigma is nonsense
(pointed out by Dale Tronrud!).

For mFo-DFc and other types of true difference Fouriers this option must
be **omitted** so that the program is allowed to recompute the standard
uncertainty.  In this case the input map must consist of **exactly one**
asymmetric unit (an integral number of a.u.'s such as **exactly** half
or a complete unit cell is also valid input).  This will normally be the
case if the **FFT** program has been used to prepare the input map.

This option should also be used in the case that the input map does not
cover an exact integral number of a.u.'s, but the SU of the density has
been previously correctly computed, so that the correct SU is kept and
copied to the output map.

END
~~~

Ends input.

INPUT AND OUTPUT FILES
----------------------

 MAPIN
    input map (from FFT)
 MAPOUT
    extended output map
 XYZIN
    input coordinates, optional unless limits not given.

PROGRAM FUNCTION
----------------

The program EXTENDS is used to extend the range of Fourier map or to
calculate a different part of the unit cell from that input.  Its main
use is in preparing data for programs that process electron density and
require an accurate value of the standard uncertainty.

AUTHORS
-------

| Phil Evans, MRC-LMB, Cambridge (original EXTEND code)
| Ian Tickle, Astex Technology, Cambridge (mods for SU calculation)

SEE ALSO
--------

`fft, <fft.html>`__\ `mapmask <mapmask.html>`__

EXAMPLES
--------

The following is an example of control data for extending a map

::


    extends MAPIN 2fofc.map MAPOUT 2fofc-ext.map << END-extends
    BORD 4
    KEEP  ! Only for nFo-(n-1)Fc type maps.
    END-extends

    extends MAPIN fofc.map MAPOUT fofc-ext.map << END-extends
    BORD 4
    END-extends
