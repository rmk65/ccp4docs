|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • `Result Pages <qtpisa-resultpages.html>`__ •
`List of Interfaces <qtpisa-intflistpage.html>`__ • `Interface Details &
Radar <qtpisa-intfdetpage.html>`__ • Chemical bonds

.. raw:: html

   </div>

The Chemical Bonds page contains the lists of hydrogen bonds, salt
bridges, disulphide bonds and covalent links formed between interfacing
`monomers <qtpisa-glossary.html#monomeric-unit>`__. The respective
interface may be identified in the `Result
Tree <qtpisa-glossary.html#pisa-result-tree>`__ as the parent node for
the currently displayed page. All lists have identical format and
represent tables with 3 columns and one row per bond:

+----------------------------------------------------------------------------+------------------------------------------------------------+----------------------------------------------------------------------------+
| Monomer 1                                                                  | Length                                                     | Monomer 2                                                                  |
+============================================================================+============================================================+============================================================================+
| `Atom ID <qtpisa-glossary.html#atom-id>`__ of bonded atom in 1st monomer   | Bond length (distance between bonded atoms) in angstroms   | `Atom ID <qtpisa-glossary.html#atom-id>`__ of bonded atom in 2nd monomer   |
+----------------------------------------------------------------------------+------------------------------------------------------------+----------------------------------------------------------------------------+

| 
|  

--------------

| ***See also***

-  `Interfacing residues <qtpisa-intfrespage.html>`__

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
