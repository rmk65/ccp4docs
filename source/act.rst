ACT (CCP4: Supported Program)
=============================

NAME
----

**act** - analyse coordinates.

SYNOPSIS
--------

| **act xyzin** *xyz\_in.pdb*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

The program does some useful coordinate analyses. For example it quickly
finds ALL the crystal contacts, no matter where the molecule is placed
with respect to the origin (otherwise the fast algorithm for finding
contacts is the same as in Tadeusz Skarzynski's program CONTACT).

The program always gives statistics for the temperature factors averaged
by residue, for the main chain and side chains.

Lists atoms with B-values .gt. bmoni (default = 50).

Lists B-values for Ox1 and Nx2 atoms for each ASN and GLN - if the
difference is large the atoms could be wrong way round.

Prints a histogram of solvent molecules in ranges of B-values.

Calculates F000 (the number of electrons in the unit cell).

The optional output includes various contact analyses.

KEYWORDED INPUT
---------------

The various data control lines are identified by keywords. Only the
first 4 letters of each keyword are necessary.

    `**BDUMP** <#bdump>`__, `**BMONITOR** <#bmonitor>`__,
    `**CONTACT** <#contact>`__, `**END** <#end>`__ (compulsory),
    `**HBOND** <#hbond>`__, `**SEQU** <#sequ>`__,
    `**SHORT** <#short>`__, `**SYMM** <#symmetry>`__ (compulsory)

SYMM <space\_group name or number>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[COMPULSORY INPUT]

CONTACT <keyword> [<dmin>] <dmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[OPTIONAL INPUT]

::


    Listing of contacts

       Keywords:
                ALL (default)
                INTERmolecular
                INTRAmolecular

                dmin dmax - distance limits for listing contacts,
                            in any order (default = 0.0, 5.0).
                            If only one value given it is assumed 
                            to be dmax.

SHORT <dmoni>
~~~~~~~~~~~~~

[OPTIONAL INPUT]

Listing of short contacts

dmoni (default = 2.5) short contacts, up to dmoni, are listed.

HBOND <keyword> <hdmin> <hdmax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[OPTIONAL INPUT]

Listing of possible hydrogen bonds (by distance criterion). <keyword> is
one of:

::

                ALL (default)
                INTERmolecular
                INTRAmolecular

<hdmin> <hdmax> (in any order) are the distance limits for listing
contacts (default = 2.7, 3.3). A list of unpaired hydrogen bonding atoms
is given with HBOND ALL option.

HBOND looks for contacts between oxygen and nitrogen atoms. An earlier
version of ``act`` excluded various N/O and O/O combinations, but this
was causing genuine hydrogen bonds to be missed. The current version
makes no such exclusions, and therefore will list many false positives.

In any case, ``act`` uses a simple distance criterion. From the list
generated, you should check:

#. protonation: does one atom act as donor and one as acceptor? This may
   depend on a wider hydrogen-bonding network
#. angles: are the bonded-donor-acceptor and donor-acceptor-bonded
   angles acceptable?

BMONITOR <bmoni>
~~~~~~~~~~~~~~~~

[OPTIONAL INPUT]

Monitor B-values greater than bmoni (default = 50.0).

BDUMP
~~~~~

[OPTIONAL INPUT]

Dump ascii plot files (BRES.PLT) for the B-factors to disk

SEQU
~~~~

[OPTIONAL INPUT]

Extract sequence and analyse the amino acid composition.

END
~~~

[COMPULSORY INPUT]

last card, ends input.

NOTE
----

PLOTTING:

If NAG graphics library is available: Uncomment the call to plotit and
the ACTPLOT code and link with the library. You can then plot B-values
by residue with keyword:

BPLT
~~~~

[OPTIONAL INPUT]

Plot B-values by residue with NAG graphics library

INPUT AND OUTPUT FILES
----------------------

The input files are:

#. Control data file
#. Input coordinate file in PDB format

The output files are:

#. Listing of program output to the printer.
#. BRES.PLT ascii plot file for the B-factors, if BDUMP keyword was
   read.

PRINTER OUTPUT
--------------

The printer output starts with F000 (the number of electrons in the unit
cell), the number of amino acid residues, solvent molecules, metal and
other atoms in the input file.

The program then outputs analysis of the temperature factors by residue
for main chain and side chains. For each chain a table is printed for
the distribution of residues in ranges of standard deviation of the
B-values. The residues with temperature factor greater than three
standard deviations from the average value are listed. This analysis is
done for main chain and side chain atoms for each chain separately.

A list is given of atoms with B-values greater than bmoni, if input, or
50 by default.

Next follows a list of B-values for the oxygens and nitrogens of the
amide groups of glutamines and asparagines. This can be used as a check
if those atoms are placed the wrong way round - the difference would
then be large.

At the end of the 'compulsory' output a histogram of the solvent
molecules is given in ranges of temperature factors

For the contact searching options pairs of atoms are listed and the
distance between then. If the contact is with a symmetry related atom
the number of NSYM symmetry operation is given for that atom followed by
the number of translations of whole unit cell along x, y and z.

For the HBOND ALL option a list of unpaired hydrogen bonding atoms is
given. For well refined structures the atoms with hydrogen bonding
potential should normally make a bond, perhaps with the exception of
main chain nitrogens of prolines.

For SEQU option the sequence is extracted from the input file and listed
for each chain in both the three-letter and one letter notations. Next
the amino acid composition is listed followed by a histogram.

EXAMPLES
--------

UNIX example script
~~~~~~~~~~~~~~~~~~~

| `$CEXAM/unix/runnable/act.exam <../examples/unix/runnable/act.exam>`__
| Example using toxd data.

AUTHORS
-------

Originators : Wojtek Rypniewski and Howard Terry

SEE ALSO
--------

Alternative B factor analysis:

-  `baverage <baverage.html>`__

Alternative distance analysis:

-  `ncont <ncont.html>`__
-  `contact <contact.html>`__
-  `distang <distang.html>`__

General references:

#. W.Kabsch and C.Sander (1983) *Biopolymers* **22**, 2577-2637
   Dictionary of Protein Secondary Structure - Pattern-recognition of
   hydrogen-bonded and geometrical features.
