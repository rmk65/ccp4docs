ICOEFL (CCP4: Supported Program)
================================

NAME
----

**icoefl** - vectorially combined scaling of Fobs (Iobs) with partial
Fc's

SYNOPSIS
--------

| **icoefl hklin** *foo\_in.mtz* **hklout** *foo.mtz*
| [`Keyworded input <#keywords>`__]

 DESCRIPTION
------------

Icoefl - Coefficients for Jack-Levitt refinement on DEL(i) or DEL(f).

This program can be used to scale together an Fobs (or Iobs) with a
number of partial Fc's, vectorially combined. The partial structure
factors might be *e.g.* from atoms and from back-transforming a solvent
mask, to do a bulk solvent correction, or for scaling together
contributions from a mixed structure.

The function minimised is

::


             Sum{ w * [ Aobs - Acalc ]**2 }
     where
                Aobs = k0*Fobs or k0*Fobs**2
               Acalc = scaled Fcalc(total) or scaled Fcalc(total)**2
      complex Fcalc  =  k1 * exp (-B1 s**2) * complex FC1 +
                        k1 * exp (-B2 s**2) * complex FC2 +
                               . . . . .

    for up to 4 partial Fc's (defined by the number assigned by LABIN)

      w = weight = 1.0/(C1 * sigma(Fobs) + C2)   (see WEIGHT command)

The output file contains Fobs and the total Fcalc, PhiCalc, scaled
either to Fobs or to one of the Fc's (see `FIX command <#fix>`__). Thus
if FC1 is the contribution from atoms, scaling to FC1 will put the
output on an absolute scale (FIX FC 1, this is the default)

KEYWORDED INPUT
---------------

The only compulsory command is LABIN. Keywords:

    `**ANALYSIS** <#analysis>`__, `**CYCLE** <#cycle>`__,
    `**DEBUG** <#debug>`__, `**END** <#end>`__,
    `**FILTER** <#filter>`__, `**FIX** <#fix>`__,
    `**INTENSITIES** <#intensities>`__, `**LABIN** <#labin>`__,
    `**LABOUT** <#labout>`__, `**OUTPUT** <#output>`__,
    `**PRINT** <#print>`__, `**REFINE** <#refine>`__,
    `**RESOLUTION** <#resolution>`__, `**RUN** <#run>`__,
    `**SCALE** <#scale>`__, `**SFALL** <#sfall>`__,
    `**TITLE** <#title>`__, `**WEIGHT** <#weight>`__

TITLE <title>
~~~~~~~~~~~~~

If this is omitted, the title will be copied from HKLIN to HKLOUT

LABIN <program\_label>=<column\_label> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(Compulsory) Column assignments for

::


      FP,  SIGFP    native F, sigma(F)
      FC1, PHIC1    1st Fcalc, PhiCalc
      FC2, PHIC2    2nd Fcalc, PhiCalc
      FC3, PHIC3    3rd Fcalc, PhiCalc
      FC4, PHIC4    4th Fcalc, PhiCalc

The number of Fc's used for scaling is defined by which ones are
assigned here. ALL required columns MUST be assigned, INCLUDING FP and
SIGFP.

LABOUT <program\_label>=<column\_label> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Column assignments to rename output columns from their default values

::


      FP, SIGFP   native F, sigma(F)
      FC, PHIC    combined Fcalc, PhiCalc
      [W]         weight (only if OUTPUT W or OUTPUT WDEL)
      [WDEL]      w*(Fobs-Fcalc) (only if OUTPUT WDEL)
      [FCn,PHICn] partial Fcalcs, Phicalcs with n=1 to 4,
                  from the input file (only if OUTPUT FPART)

If LABOUT is omitted then the labels from LABIN are used (with W and
WDEL being the defaults for the weights). See also `LABIN <#labin>`__
and `OUTPUT <#output>`__.

CYCLE <number\_of\_cycles>
~~~~~~~~~~~~~~~~~~~~~~~~~~

Read number of cycles of refinement [default = 8]. The refinement can
converge quite slowly if starting a long way from correct values.

OUTPUT [ NOHKL \| NONE \| FOFC \| WEIGHT \| WDEL \| FPART ] [ BKR ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Controls what goes into the output file HKLOUT:

     NOHKL
        no output file
     NONE
        no o/p file and no final statistics
     FOFC
        output contains FP, SIGFP, FC, PHIC (default)
     WEIGHT
        output also contains WEIGHT
     WDEL
        output also contains WEIGHT WDEL (= w(Fo-Fc))
     FPART
        output also contains scaled component FCn, PHIn

The program can optionally output a file containing the refined scales
and B-factors, to logical name RSTATSBKR: this is controlled by the
keyword BKR:

     BKR
        write output file RSTATSBKR (NORST=0) [default no output]

SCALE <label> <scale> <B> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Read starting scales and B-factors. <label> is one of FP, FC1, FC2 etc.

| It is a good idea to start the parameters at approximately the right
  values. B for FP (=Fobs) will ignored
| [Default scales = 1.0, B's = 0.0]

::

          e.g.    SCALE FP 0.8
                  SCALE FC2  1.2 80

FIX [ FOBS \| FC <n> ]
~~~~~~~~~~~~~~~~~~~~~~

Define which F to set = 1.0 for output scaling:

     FOBS
        keep FOBS constant, apply scale to all Fc's (KEEP=0).
     FC <n>
        keep <n>-th Fcalc constant (KEEP = n). This is the default with
        <n> = 1.

With the default value (= FIX FC 1) the output FP and FC are on the
scale of FC1.

SFALL
~~~~~

The output Fobs are unscaled. Fc's are scaled relative to Fc(keep) (see
keyword `FIX <#fix>`__)

REFINE <refine\_subkey> <parameter> <n> [ <m> ] ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set which parameters are fixed, variable or tied together during the
refinement.

**<refine\_subkey>**
    one of FREE, FIX or TIE.
**<parameter>**
    either FC (calculated structure factor) or BFC (temperature factor).
**<n>**, **<m>**
    In the case of FREE and FIX, a single numerical argument **<n>** is
    required after FC/BFC, to specify which set of calculated structure
    factors are used. For TIE, two arguments are required, **<n>** and
    **<m>**, which specify that the nth parameter is tied to the mth
    parameter (which must remain FREE).

By default, all parameters are free. An example or two should clarify
the situation:

::

          REFINE FIX FC 1
    or
          REFINE FIX BFC 1 FIX BFC 2
    or
          REFINE TIE FC 1 2 FIX FC 3

are all valid. Multiple **<refine\_subkey>**\ s are allowed as long as
they follow the correct syntax; alternatively, REFINE may be used
multiple times (so not all the parameters need to be specified on a
single line).

RESOLUTION <x1> [ <x2> ]
~~~~~~~~~~~~~~~~~~~~~~~~

<x1>, <x2> are resolution limits in Angstroms, either order.

Set resolution limits. By default, all data are used and the limits are
taken from the HKLIN file

WEIGHT <C1> <C2>
~~~~~~~~~~~~~~~~

| Read constants defining weight = 1/(<C1>\*sigma(Fobs)+<C2>)\*\*2
| Default <C1>=0, <C2>=1, *i.e.* unit weights

RUN
~~~

Turn output off, and set flag to return to reading command input after
refinement

This command can be used to allow refinement of some parameters, then a
return to reading new commands to change which parameters are refined,
*e.g.*

::


      REFINE FIX FC 2
      RUN
      REFINE FREE FC 2
      OUTPUT FOFC
      END

ANALYSIS [F]
~~~~~~~~~~~~

The command ANALYSIS F produces additional analyses on F as well as the
default analyses on resolution

INTENSITIES
~~~~~~~~~~~

Set flag to refine on I rather than F [default = F]. Iobs is calculated
from FP\*\*2

PRINT <n>
~~~~~~~~~

Number of output lines to print [default = 0]

DEBUG <n>
~~~~~~~~~

Set debug flag [default = 0]

FILTER [ <ifilt> ] [ <neigmx> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set filter levels for least-squares.

     IFILT
        filter level = 10\*\*(-IFILT) [default = 8]
     NEIGMX
        maximum number of eigenvalues to use [default = 0 = all]

END
~~~

Finish input and let the program run to termination. End-of-file is
equivalent to the END keyword.

 EXAMPLES
---------

::

    icoefl hklin u1afofcfslv  
           hklout u1afofcfslv_sc  
           <<eof-icoefl
    title Scale Fc(atom) and Fc(solvent) to Fobs
    labin FP=FP SIGFP=SIGFP FC1=FC PHIC1=PHIC FC2=FCSOLV PHIC2=PHICSOLV
    labout  FC=FCALL PHIC=PHICALL
    scale FP 1.4
    scale FC1 1.0 15
    scale FC2 0.3 80
    cycles  20
    eof-icoefl

AUTHORS
-------

Author: Judd Fermi, MRC LMB, Cambridge

MTZ version: Phil Evans, MRC LMB, Cambridge
