CCP4 v6.0 Acknowledgements
==========================

CCP4 relies on the contribution of programs and software from many
protein crystallographers. We would like to thank all contributing
software authors for their efforts and their generosity in allowing us
to distribute their code, thus making it available to a wide community
of users. Details of authors of individual programs are given in the
corresponding `program documentation <INDEX.html>`__.

A large number of people have also given their time and energy to test
this version of the CCP4, and to report and fix bugs. We would like to
acknowledge these people (in no particular order):

-  Ezra Peisach (Brandeis University)
-  Kevin Cowtan, Paul Emsley, Liz Potterton, Maria Turkenburg, Johan
   Turkenburg (University of York)
-  Eleanor Dodson (University of York)
-  Phil Evans (MRC Cambridge)
-  Randy Read, Airlie McCoy (Cambridge University)
-  Clemens Vonrhein (Global Phasing)
-  Ralph Grosse-Kunstleve (Lawrence Berkeley Laboratory)
-  Raj Pannu, Steven Ness (University of Leiden)
-  Alun Ashton (Diamond Light Source)
-  Nick Keep (Birkbeck)
-  Matsuura Takanori (Osaka)
-  Graeme Winter (Daresbury)
-  Bill Scott (UCSC)
-  Peter Moody (University of Leicester)
-  David Schuller (Cornell)
-  Jim Naismith (University of St Andrews)
-  Tadeusz Skarzynski (GSK)
-  Huanwang Yang (RCSB/PDB)
-  Alejandro Buschiazzo (Institut Pasteur)
-  Bennet Fauber (University of Michigan)

Finally, we would like to thank all those people who made contributions
which are not explicitly acknowledged here, which includes all program
authors who contributed new or updated versions of their software. CCP4
remains a collaborative effort and as such these releases would not
possible without your help and assistance.

**Thank you!**

--------------

*On behalf of the CCP4 group at CCLRC Daresbury Laboratory
Peter Briggs, Charles Ballard, Martyn Winn, Francois Remacle, Ronan
Keegan, Norman Stein, Daniel Rolfe and Maeri Howard
September 2005*

--------------
