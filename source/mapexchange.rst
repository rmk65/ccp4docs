MAPEXCHANGE (CCP4: Unsupported Program)
=======================================

NAME
----

**mapexchange** - convert map between binary and formatted

SYNOPSIS
--------

**mapexchange mapin** *foo\_in.map* **mapout** *foo\_out.map*

DESCRIPTION
-----------

The program converts input map from binary (CCP4 format) to formatted
(ASCII) and vice versa. The type of input map is determined
automatically.

INPUT AND OUTPUT FILES
----------------------

MAPIN Input map (binary or formatted)

MAPOUT Output map (formatted if input map was binary and vice versa)

AUTHORS
-------

Originator : Phil Evans

Availability : Fortran 77

Contact : Phil Evans, MRC-LMB Cambridge

EXAMPLES
--------

The following is an example of control data

::


     Sample Unix input file:

    #!/bin/csh
    #
    mapexchange MAPIN some_map.map MAPOUT some_map.ascii

Date: 12 Feb 1993

From: WOJTEK@de.EMBL-Hamburg
