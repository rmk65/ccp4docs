DNA\_OUTPUT (DNA: Library)
==========================

NAME
----

**dna\_output** - DNA library for XML output from programs

DESCRIPTION
-----------

**dna\_output** is a set of subroutines for writing XML output from CCP4
programs, to interact with the DNA scheduler. The form of the XML is
very simple, with only four tags:

-  dna\_tables
-  table
-  list
-  item

which may have an attribute for the name, another for the index. In
addition there is room in there for the program name in the dna\_tables
element.

Here is an example of some program output which uses this:

::

          character dna_filename*80
          call ugtenv('DNA', dna_filename)
          if(dna_filename .ne. '') then
             call dna_start(dna_filename, 'scala')
          else
             call dna_set_no_output
          end if

          call dna_table_start('header_info')
          call dna_list_start('version_information')
          call dna_character_item('version', versn(1:lenstr(versn)))
          call dna_character_item('author', 'Phil Evans')
          call dna_character_item('date', vrdate(1:lenstr(vrdate)))
          call dna_list_end
          call dna_table_end
          call dna_end

This would obviously produce a very small XML output file, but it does
demonstrate the principle. subroutines are named by the data type they
are writing, for instance dna\_real\_item, dna\_integer\_item.

FILES
-----

::

    $CLIBS/dna_output.f
    $CLIBS/dna_header.f

AUTHOR
------

Graeme Winter

SEE ALSO
--------

-  The DNA Project http://www.dna.ac.uk/
