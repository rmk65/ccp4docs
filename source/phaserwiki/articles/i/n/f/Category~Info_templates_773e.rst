.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Category:Info templates
   :name: categoryinfo-templates
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div class="noarticletext">

There is currently no text in this page. You can `search for this page
title <../../../../articles/s/e/a/Special%7ESearch_Info_templates_0822.html>`__
in other pages, `search the related
logs <http://localhost../../../../articles/l/o/g/Special%7ELog_01d8.html>`__,
or `edit this
page <http://localhost../../../../articles/i/n/f/Category%7EInfo_templates_773e.html>`__.

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-pages">

.. rubric:: Pages in category "Info templates"
   :name: pages-in-category-info-templates

This category contains only the following page.

.. rubric:: M
   :name: m

-  `Template:Mediawiki <../../../../articles/m/e/d/Template%7EMediawiki_2519.html>`__

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-site" class="portlet">

.. rubric:: site
   :name: site

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Home">

   .. raw:: html

      </div>

   `Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-News">

   .. raw:: html

      </div>

   `News <../../../../articles/n/e/w/News.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Contact">

   .. raw:: html

      </div>

   `Contact <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Events">

   .. raw:: html

      </div>

   `Events <../../../../articles/e/v/e/Events.html>`__
-  

   .. raw:: html

      <div id="n-Developers">

   .. raw:: html

      </div>

   `Developers <../../../../articles/d/e/v/Developers.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-documentation" class="portlet">

.. rubric:: documentation
   :name: documentation

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Molecular-Replacement">

   .. raw:: html

      </div>

   `Molecular
   Replacement <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-Experimental-Phasing">

   .. raw:: html

      </div>

   `Experimental
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Bugs">

   .. raw:: html

      </div>

   `Bugs <../../../../articles/b/u/g/Bugs.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__
-  

   .. raw:: html

      <div id="n-Help">

   .. raw:: html

      </div>

   `Help <../../../../articles/c/o/n/Help%7EContents_22de.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

Static
`Phaserwiki <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   </div>
