.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Tutorials
   :name: tutorials
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

Example Scripts
    Copy and edit to start using Phaser

    -  `Keyword Example
       Scripts <../../../../articles/k/e/y/Keyword_Example_Scripts.html>`__
    -  `Python Example
       Scripts <../../../../articles/p/y/t/Python_Example_Scripts.html>`__

Tutorials
    The tutorials are suitable for individual use or class teaching.

    -  `MR using
       CCP4i:TOXD <../../../../articles/t/o/x/MR_using_CCP4i:TOXD.html>`__
    -  `MR using
       CCP4i:BETA/BLIP <../../../../articles/b/e/t/MR_using_CCP4i:BETA/BLIP.html>`__
    -  `MR using keyword
       input <../../../../articles/m/r/_/MR_using_keyword_input.html>`__
    -  `Finding a search
       model:TOXD <../../../../articles/t/o/x/Finding_a_search_model:TOXD.html>`__
    -  `Molecular replacement with
       MRage <../../../../articles/m/o/l/Molecular_replacement_with_MRage.html>`__
    -  `SAD using
       CCP4i <../../../../articles/s/a/d/SAD_using_CCP4i.html>`__
    -  `Combined MR-SAD using
       CCP4i <../../../../articles/c/o/m/Combined_MR-SAD_using_CCP4i.html>`__
    -  `EP using
       Phenix <../../../../articles/e/p/_/EP_using_Phenix.html>`__
    -  `SCEDS using
       CCP4i <../../../../articles/s/c/e/SCEDS_using_CCP4i.html>`__
    -  `SCEDS for
       MR <../../../../articles/s/c/e/SCEDS_for_MR_(using_keyword_input).html>`__
    -  `NMA Coordinate Perturbation using
       CCP4i <../../../../articles/n/m/a/NMA_Coordinate_Perturbation_using_CCP4i.html>`__

Coot script for Normal Mode Analysis - Perturbations (NMAXYZ)
    To add a button to Coot to convert all loaded molecules to CA
    representation (C-alphas/backbone in the Display Manager) put the
    following script (called e.g. all-to-ca.py) in your
    .coot-preferences directory. Jiffy courtesy of Paul Emsley.

::

     def all_molecules_to_CA():
       for imol in model_molecule_list():
           graphics_to_ca_representation(imol)
     coot_toolbar_button("CA", "all_molecules_to_CA()", icon_name="6-c.svg")
     coot_toolbar_button("pre-CA", "set_default_representation_type(4)",icon_name="5-c.svg")

| 

Data
    The coordinate (pdb), sequence (fasta) and reflection (mtz) files
    required to run the MR tutorials are distributed with Phaser.

|Download.png| `Data for Molecular Replacement Examples and
Tutorials <http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/phaser-mr-tutorial.tar.gz>`__
|Download.png| `Data for Experimental Phasing Examples and
Tutorials <http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/phaser-ep-tutorial.tar.gz>`__
|Download.png| `Data for SCEDS Examples and
Tutorials <http://www-structmed.cimr.cam.ac.uk/phaser/tutorial/phaser-sceds-tutorial.tar.gz>`__

References
    The crystal structure of a hetero-dimer of beta-lactamase (BETA) and
    beta-lactamase inhibitor protein (BLIP), both with molecular
    replacement models from crystal structures of the individual BETA
    and BLIP components. We thank Mike James and Natalie Strynadka for
    the diffraction data. Reference: Strynadka, N.C.J., Jensen, S.E.,
    Alzari, P.M. & James. M.N.G. (1996) *Nat. Struct. Biol.* **3**
    290-297.
    The crystal structure of insulin phased on intrinsic sulphurs. We
    thank Paul Adams for the diffraction data. Reference: Adams (2001)
    *Acta Cryst* D\ **57**. 990-995.

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Tutorials&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Tutorials>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 12:16, 6 April 2016 by `Randy
   Read <../../../../articles/r/a/n/User:Randy.html>`__. Based on work
   by `Airlie McCoy <../../../../articles/a/i/r/User:Airlie.html>`__,
   `Gabor Bunkoczi <../../../../articles/g/a/b/User:Gaborb.html>`__ and
   `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Download.png| image:: ../../../../images/f/2F/f/f/f1/Download.png
   :width: 48px
   :height: 48px
.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
