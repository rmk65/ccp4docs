.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Finding a search model:TOXD
   :name: finding-a-search-modeltoxd
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

Tutorial authored by Nick Keep

Models for molecular replacement are found by database searching. When
solving a new structure, you would need to search with your sequence(s)
against the protein databank to find a model structure. This can be done
in a number of places. The toxd sequence is

QPRRKLCILHRNPGRCYDKIPAFYYNQKKKQCERFDWSGCGGNSNRFKTIEECRRTCIG

#. Go to the search page on the RCSB website
   http://www.rcsb.org/pdb/search/advSearch.do
#. Find the option to search by sequence in the pull-down menu and paste
   the sequence into the query window, and run the search
#. The top hit will be the structure of toxd itself. In real life you
   would be disappointed to find your target structure at the top of the
   list! Download the three lower homology models 1DEN, 1BF0 and 1TFX
   onto your computer. There are a number of different ways to do this:

   -  Click on the text icon and then when the text window opens, use
      the right mouse to save page to disk, or
   -  Click on the icon with the pale blue download arrow, which will
      give you a gzipped pdb file. You will need to unzip this file
      before using it.

#. Take note of the homology and the chain(s) to which the homology
   applies
#. View the structures in a graphics program (e.g. rasmol, xtalview,
   coot)
#. For one of the PDB files you will need to select the chains you want
   for your model. You can just edit the PDB file in a text editor or
   you can use the Edit PDB File utility in ccp4i under Coordinate
   Utilities. Select pdbset as the program to perform a selection for
   output as a pdb file. You need to check select by chain name and
   enter the chain of choice.
#. It is a good idea to remove waters from a PDB file before using it as
   a model, as these are unlikely to be structurally conserved between
   model and target. Phaser removes these automatically, but other
   molecular replacment programs do not. Run pdbset on all three files
   as above but this time to Exclude water molecules.
#. View the edited structures in a graphics program (e.g. rasmol,
   xtalview, coot) to double check that you have the model you intended
   to get
#. Try the molecular replacement in the `TOXD
   tutorial <http://www.phaser.cimr.cam.ac.uk/index.php/MR_using_CCP4i:TOXD>`__
   with the above search models

   -  How does Phaser handle the multiple models given for an NMR
      structure?

#. Is the success of molecular replacement related to sequence identity?

   -  Consider the structure solution method. Why do NMR models make
      poor molecular replacement models?

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks">

.. raw:: html

   <div id="mw-normal-catlinks">

`Category <../../../../articles/c/a/t/Special%7ECategories_101d.html>`__:
`Tutorial <../../../../articles/t/u/t/Category%7ETutorial_c3b5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
