.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Phaser Crystallographic Software
   :name: phaser-crystallographic-software
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

Phaser is a program for phasing macromolecular crystal structures with
maximum likelihood methods. It has been developed by `Randy Read's
group <articles/d/e/v/Developers.html>`__ at the `Cambridge Institute
for Medical Research <http://www.cimr.cam.ac.uk>`__ (CIMR) in the
`University of Cambridge <http://www.cam.ac.uk>`__ and is available
through the `Phenix <http://www.phenix-online.org>`__ and
`CCP4 <http://www.ccp4.ac.uk>`__ software suites.

Use the **sidebar** to navigate through the extensive documentation for
Phaser.

*This PhaserWiki supersedes the obsolete `Phaser
homepage <http://www-structmed.cimr.cam.ac.uk/phaser/>`__, which now
redirects to this wiki. A copy of the obsolete Phaser homepage can be
found `here <http://www-structmed.cimr.cam.ac.uk/phaser_obsolete/>`__*

.. rubric:: Currently Supported Releases
   :name: currently-supported-releases

.. rubric:: Phaser-2.7.14
   :name: phaser-2.7.14

Download with Phenix Nightly Builds →
`Phenix <http://www.phenix-online.org/download/>`__
Download with CCP4 Nightly Builds →
`CCP4 <http://devtools.fg.oisin.rc-harwell.ac.uk/nightly/>`__
Documentation → `Manual <articles/m/a/n/Phaser-2.7.14:_Manual.html>`__
*Changes*

-  SOLUTION HISTORY tracks solution through positions in RF/TF/PAK/RNP
   peak lists
-  selection by CHAIN and MODEL for PDB coordinate entry
-  automatic search number for single search ensemble
-  packing 'trace' molecule can be entered independently of coordinates
   and map
-  read TNCS/anisotropy binary files to avoid refinement, when running
   through scripts
-  write tNCS and anisotropy parameters to binary files (non-python
   interface)
-  default reading of I (or failing that, F) from mtz file (LABIN
   optional)
-  trace for ensembles from maps = hexgrid of 1000+/-100 points
-  trace for ensembles from coordinates above 1000 Calpha = hexgrid of
   1000+/-100 points
-  trace for ensembles from coordinates twixt 1000 atoms and 1000 Calpha
   = Calpha atoms
-  trace for ensembles from coordinates under 1000 atoms = all atoms
-  packing by pairwise percent only, other packing modes obsoleted
-  packing test during FTF run by default with 50% pairwise packing
   cutoff
-  automatic tNCS NMOL determination in presence of commensurate
   modulation
-  added MODE GIMBLE, which splits ensembles by chain for rigid body
   refinement
-  support for unicode
-  solution coordinates placed nearest to input coordinates if possible

.. rubric:: Phaser-2.6.0
   :name: phaser-2.6.0

Download with Phenix Official Release 1.10 (September 2015) →
`Phenix <http://www.phenix-online.org/download/>`__
Download with CCP4 7.0 → `CCP4 <http://www.ccp4.ac.uk/download.php>`__
Documentation → `Manual <articles/m/a/n/Phaser-2.6.0:_Manual.html>`__
| 

.. rubric:: Referencing Phaser
   :name: referencing-phaser

Citing crystallographic software in your paper is important for funding
new software development. We rely on your citations to convince funding
bodies that our software is being used.

If you solve a structure with Phaser, please cite

Phaser crystallographic software
    McCoy AJ, Grosse-Kunstleve RW, Adams PD, Winn MD, Storoni LC, Read
    RJ.
    `**J Appl Cryst** <http://scripts.iucr.org/cgi-bin/paper?he5368>`__
    (2007). 40, 658-674.

| 

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <index.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Phaser_Crystallographic_Software&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest
   revision <http://localhost/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR Phasing <articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD Phasing <articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python Interface <articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 12:40, 13 September 2016 by `Airlie
   McCoy <articles/a/i/r/User:Airlie.html>`__. Based on work by
   `WikiSysop <articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About Phaserwiki <articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ./skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
