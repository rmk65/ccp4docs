|previous section| | | |contents page| | | |next appendix| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Appendix C - Program descriptions
=================================

Program 1. CLEAN.F
------------------

**Written by: D K Smith & E G Hutchinson. Modified: R A Laskowski**

The first of the programs is called **CLEAN**. Its purpose is to produce
a cleaned-up version of your coordinates file. The new file is given a
**.new** extension and is used by the other programs in the suite for
the calculation of the stereochemical parameters.

The cleaning-up process involves a number of checks. The first one
ensures that the atoms in your structure have been correctly labelled in
accordance with the **IUPAC** naming conventions (`IUPAC-IUB Commission
on Biochemical Nomenclature, 1970 <manrefs.html#IUPAC>`__).

A typical error is that the **Neta1** and **Neta2** atoms of arginine
are labelled the wrong way round. This error is corrected by the
program. Also corrected are atom labels for **Phe**, **Tyr**, **Asp**
and **Glu** residues. Thus, for example, the carbon atom in tyrosine
labelled **Cdelta1** is the one that gives the lowest **chi-2** torsion
angle.

The program switches atom labels when they are in error and shows the
old atom names in a column at the right of the **.new** file.

The program also checks that the correct **L/D** stereochemical
assignments have been made on individual residues, and corrects any
errors found.

All other errors are merely flagged in the **.new** file and described
in the **clean.log** file. Also listed in **clean.log** are: chain
breaks, unknown residues, and missing atoms. Chain breaks are defined as
locations where the distance between two adjacent Calpha atoms is
greater than **5.0Å**. Residue atoms on one side of such a break are
marked with a **\`>'** in the **.new** file while the atoms of the
residue on the other side are marked with a **\`<'**.

Atoms with alternate locations have only the location with the highest
occupancy retained in the **.new** file. The alternate positions are
written to the .new file, but are flagged: **ATOM** records are marked
as **ATALT** and **HETATM** records as **HEALT**. Atoms with zero
occupancy are flagged as **ATZERO** and **HEZERO**. None of the flagged
atoms are included in the stereochemical checks.

Finally, residues with unusual values of the "notional" **zeta**
dihedral angle (defined by the four atoms: **Calpha-N-C-Cbeta**) are
marked with an asterisk in the .new file. The value of this angle should
be > **23** degrees and < **45** degrees.

Program 2. SECSTR.F
-------------------

**Written by: D K Smith. Modified: R A Laskowski**

The second program in the suite is **SECSTR** which calculates all the
required torsion angles, main-chain hydrogen-bond energies, and
secondary structure assignments. This information is written out to the
**.rin** file for use by the plotting programs.

The hydrogen-bond energies and the secondary structure assignments are
calculated using the method of `Kabsch & Sander
(1983) <manrefs.html#KABSCH>`__.

Program 3. NB.C
---------------

**Written by: D T Jones & D Naylor**

The third program in the suite is called **NB**. It identifies all
non-bonded interactions between different pairs of residues in the
protein structure. These are defined where the closest atom-atom contact
between two residues is less than **2.0Å** and the atoms concerned are
**4** or more bonds apart.

All such closest contacts are written out to the **.nb** file for use by
the plotting programs.

**Note that**, only intra-chain contacts are stored - inter-chain
contacts are not considered.

Program 4. ANGLEN.F
-------------------

**Written by: R A Laskowski**

The fourth program in the suite is called **ANGLEN**. It calculates all
the main-chain bond lengths and bond angles in the structure. These are
written out to the **.lan** file. Also calculated are the **RMS**
distances from a best-fit plane for all planar sidechain groups. These
comprise the **aromatic rings** of Phe, Tyr, Trp and His, and the
**end-groups** of Arg, Asn, Asp, Gln and Glu. The coordinates of the
atoms in the planar groups are written out to the **.pln** file.

Programs 5-7. TPLOT.F, PPLOT.F and BPLOT.F
------------------------------------------

**Written by: R A Laskowski & M W MacArthur**

The last 3 programs in the suite are the plotting programs which create
the plots and the residue-by-residue listing for your structure. The
plots and listing have a number of user-definable options which are held
in the parameter file **procheck.prm and** which can be changed simply
by editing the file using any text editor (see `Customizing the PROCHECK
plots <man5.html>`__). The plots are described in `Sample
plots <examples/index.html>`__, and the print-out is described in
`Appendix D <manappd.html>`__.

--------------

|previous section| | | |contents page| | | |next appendix| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous section| image:: leftb.gif
   :target: manappb.html
.. | | image:: 12p.gif
.. |contents page| image:: uupb.gif
   :target: index.html
.. |next appendix| image:: rightb.gif
   :target: manappd.html
