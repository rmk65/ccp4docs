|previous listing| | | |plots listing| | | |next plot| | |\ PROCHECK sample plots
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Plot 2. Ramachandran plots by residue type
==========================================

|image4|

Description
-----------

The plot shows separate Ramachandran plots are shown for each of the 20
different amino acid types.

The **darker** the shaded area on each plot, the **more favourable** the
region. The data on which the shading is based has come from a data set
of **163** non-homologous, high-resolution protein chains chosen from
structures solved by X-ray crystallography to a resolution of **2.0Å**
or better and an *R*-factor no greater than **20%**.

The **numbers in brackets**, following each residue name, show the total
number of data points on that graph. The **red numbers** above the data
points are the **reside-numbers** of the residues in question (*ie*
showing those residues lying in unfavourable regions of the plot).

Options
-------

The main options for the plot are:-

-  Ramachandran plots for `**Gly & Pro** residues
   only <plot_02a.html>`__.
-  The cut-off value for the *G*-factor defining which points are to be
   labelled.
-  The plot can be in colour or black-and-white.

These options can be altered by editing the parameter file,
**procheck.prm**, as described `here <../parameters/manopt_02.html>`__.

--------------

|previous listing| | | |plots listing| | | |next plot| | | *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous listing| image:: ../leftr.gif
   :target: plot_01.html
.. | | image:: ../12p.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_03.html
.. |image4| image:: plot_02.gif
   :width: 306px
   :height: 434px
   :target: plot_02.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_03.html
