|previous listing| | | |plots listing| | | |next plot| | |\ *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Plot 9. RMS distances from planarity
====================================

|image4|

Description
-----------

These histograms show the **RMS** distances from planarity for the
different planar groups in the structure. The dashed lines indicate
different ideal values for aromatic rings (Phe, Tyr, Trp, His) and for
planar end-groups (Arg, Asn, Asp, Gln, Glu). The default values are
**0.03Å** and **0.02Å**, respectively, but these values can be altered
by editing the `procheck.prm <../parameters/manopt_09.html>`__ file.

Histogram bars beyond the dashed lines are shown as highlighted.

Options
-------

The main options for the plot are:-

-  RMS distance from planarity for highlighting outliers for ring groups
   (default is **0.03Å**).
-  RMS distance from planarity for highlighting outliers for other
   groups (default is **0.02Å**).
-  The plot can be in colour or black-and-white.

These options can be altered by editing the parameter file,
**procheck.prm**, as described `here <../parameters/manopt_09.html>`__.

--------------

|previous listing| | | |plots listing| | | |next plot| | | *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous listing| image:: ../leftr.gif
   :target: plot_08.html
.. | | image:: ../12p.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_10.html
.. |image4| image:: plot_09.gif
   :width: 306px
   :height: 434px
   :target: plot_09.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_10.html
